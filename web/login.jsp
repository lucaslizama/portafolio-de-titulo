<%-- 
    Document   : login
    Created on : May 1, 2018, 2:50:16 AM
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="css/template.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    </head>
    <body class="container">
        <header>
            <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="/">Los Yuyitos</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="/">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/catalogo">Catalogo</a>
                        </li>
                    </ul>
                    <form class="form-inline">
                        <a class="btn btn-primary btn-outline-success" href="login" role="button">Login</a>
                    </form>
                </div>
            </nav>
        </header>
            <div class="row mt-5">
                <form class="mt-5 col-lg-5 mx-auto" method="POST" action="login">
                    <div class="form-group">
                      <label for="user">Nombre de Usuario</label>
                      <input type="text" class="form-control" id="user" id="user" aria-describedby="userMensaje" placeholder="Nombre de Usuario">
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" id="password" name="password" aria-describedby="passMensaje" placeholder="Contraseña">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="recordarme" name="recordarme">
                        <label class="form-check-label" for="recordarme">Check me out</label>
                    </div>
                    <span style="${color}">${mensaje}</span>
                    <button type="submit" class="btn btn-primary">Login</button>
                </form>
            </div>
        <footer class="text-muted text-center">
            <div class="container">
              <p class="float-right">
                <a href="#">Back to top</a>
              </p>
              <p>Almacen Los Yuyitos 2018 / Creado por: Lucas Lizama</p>
              <p>Proyecto Portafolio de Titulo</p>
            </div>
        </footer>
    </body>
</html>
