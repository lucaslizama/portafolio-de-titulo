<%-- 
    Document   : register
    Created on : May 1, 2018, 6:19:30 PM
    Author     : lucas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="row mt-5">
            <form class="mt-5 col-lg-5 mx-auto" method="POST" action="register">
                <div class="form-group">
                  <label for="user">Nombre de Usuario</label>
                  <input type="text" class="form-control" id="user" id="user" placeholder="Nombre de Usuario">
                </div>
                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" name="password" aria-describedby="passMensaje" placeholder="Contraseña">
                </div>
                <span style="${color}">${mensaje}</span>
                <button type="submit" class="btn btn-primary">Registro</button>
            </form>
        </div>
    </body>
</html>
