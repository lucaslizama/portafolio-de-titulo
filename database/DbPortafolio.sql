drop database if exists portafolio;
CREATE database portafolio;
use portafolio;

create table roles (
    id int auto_increment primary key,
    nombre varchar(255) not null,
    descripcion varchar(255) not null
);

create table datos_personales_usuario (
    id int auto_increment primary key,
    rut int unsigned not null unique,
    nombre varchar(255) not null,
    apellidoPaterno varchar(255) not null,
    apellidoMaterno varchar(255) not null
);

create table usuarios (
    id int auto_increment primary key,
    id_rol int not null,
    id_datos_personales int not null,
    username varchar(255) not null unique,
    pass varchar(255) not null,
    foreign key (id_rol) references roles(id),
    foreign key (id_datos_personales) references datos_personales_usuario(id)
);

create table proveedores (
    id int auto_increment primary key,
    nombre varchar(255) not null
);

create table tipos_producto(
    id int auto_increment primary key,
    nombre varchar(255) not null,
    descripcion varchar(255)
);

create table detalle_productos (
    id int auto_increment primary key,
    id_tipo_producto int not null,
    nombre varchar(255) not null,
    precio int not null,
    descripcion varchar(255) not null,
    thumbnail_url varchar(255) not null,
    foreign key (id_tipo_producto) references tipos_producto(id)
);

create table productos (
    id int auto_increment primary key,
    id_detalle int not null,
    foreign key (id_detalle) references detalle_productos(id)
);

create table estados_ventas (
    id int auto_increment primary key,
    nombre varchar(255) not null
);

create table ventas (
    id int auto_increment primary key,
    id_usuario int not null,
    id_estado int not null,
    foreign key (id_usuario) references usuarios(id),
    foreign key (id_estado) references estados_ventas(id)
);

create table detalle_venta (
    id int auto_increment primary key,
    id_venta int not null,
    id_producto int not null,
    cantidad int unsigned not null,
    foreign key (id_venta) references ventas(id),
    foreign key (id_producto) references productos(id)
);


create table pedidos (
    id int auto_increment primary key
);

create table detalle_pedidos (
    id int auto_increment primary key,
    id_pedido int not null,
    id_producto int not null,
    id_proveedor int not null,
    cantidad int unsigned not null,
    foreign key (id_pedido) references pedidos(id),
    foreign key (id_producto) references productos(id),
    foreign key (id_proveedor) references proveedores(id)
);

insert into roles (nombre,descripcion) 
values ("Administrador","El administrador del sistema"), ("Cliente","Cliente de la tienda");
insert into datos_personales_usuario (rut, nombre, apellidoPaterno, apellidoMaterno)
values (0, 'Lucas', 'Lizama', 'Monje');
insert into usuarios (id_rol, id_datos_personales, username, pass)
values (1, 1, 'lucaslizama', '$2a$12$7aa0qIJCux03gukCOTHp3eNmNWrOmvGkrRrcAEC8RUnmWKBJIIFii');