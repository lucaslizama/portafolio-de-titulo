/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "tipos_producto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposProducto.findAll", query = "SELECT t FROM TiposProducto t")
    , @NamedQuery(name = "TiposProducto.findById", query = "SELECT t FROM TiposProducto t WHERE t.id = :id")
    , @NamedQuery(name = "TiposProducto.findByNombre", query = "SELECT t FROM TiposProducto t WHERE t.nombre = :nombre")
    , @NamedQuery(name = "TiposProducto.findByDescripcion", query = "SELECT t FROM TiposProducto t WHERE t.descripcion = :descripcion")})
public class TiposProducto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoProducto")
    private List<DetalleProductos> detalleProductosList;

    public TiposProducto() {
    }

    public TiposProducto(Integer id) {
        this.id = id;
    }

    public TiposProducto(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<DetalleProductos> getDetalleProductosList() {
        return detalleProductosList;
    }

    public void setDetalleProductosList(List<DetalleProductos> detalleProductosList) {
        this.detalleProductosList = detalleProductosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposProducto)) {
            return false;
        }
        TiposProducto other = (TiposProducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.TiposProducto[ id=" + id + " ]";
    }
    
}
