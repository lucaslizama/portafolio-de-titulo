/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "datos_personales_usuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DatosPersonalesUsuario.findAll", query = "SELECT d FROM DatosPersonalesUsuario d")
    , @NamedQuery(name = "DatosPersonalesUsuario.findById", query = "SELECT d FROM DatosPersonalesUsuario d WHERE d.id = :id")
    , @NamedQuery(name = "DatosPersonalesUsuario.findByRut", query = "SELECT d FROM DatosPersonalesUsuario d WHERE d.rut = :rut")
    , @NamedQuery(name = "DatosPersonalesUsuario.findByNombre", query = "SELECT d FROM DatosPersonalesUsuario d WHERE d.nombre = :nombre")
    , @NamedQuery(name = "DatosPersonalesUsuario.findByApellidoPaterno", query = "SELECT d FROM DatosPersonalesUsuario d WHERE d.apellidoPaterno = :apellidoPaterno")
    , @NamedQuery(name = "DatosPersonalesUsuario.findByApellidoMaterno", query = "SELECT d FROM DatosPersonalesUsuario d WHERE d.apellidoMaterno = :apellidoMaterno")})
public class DatosPersonalesUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rut")
    private int rut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "apellidoPaterno")
    private String apellidoPaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "apellidoMaterno")
    private String apellidoMaterno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDatosPersonales")
    private List<Usuarios> usuariosList;

    public DatosPersonalesUsuario() {
    }

    public DatosPersonalesUsuario(Integer id) {
        this.id = id;
    }

    public DatosPersonalesUsuario(Integer id, int rut, String nombre, String apellidoPaterno, String apellidoMaterno) {
        this.id = id;
        this.rut = rut;
        this.nombre = nombre;
        this.apellidoPaterno = apellidoPaterno;
        this.apellidoMaterno = apellidoMaterno;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getRut() {
        return rut;
    }

    public void setRut(int rut) {
        this.rut = rut;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    @XmlTransient
    public List<Usuarios> getUsuariosList() {
        return usuariosList;
    }

    public void setUsuariosList(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DatosPersonalesUsuario)) {
            return false;
        }
        DatosPersonalesUsuario other = (DatosPersonalesUsuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.DatosPersonalesUsuario[ id=" + id + " ]";
    }
    
}
