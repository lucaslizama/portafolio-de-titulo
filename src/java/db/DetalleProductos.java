/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "detalle_productos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetalleProductos.findAll", query = "SELECT d FROM DetalleProductos d")
    , @NamedQuery(name = "DetalleProductos.findById", query = "SELECT d FROM DetalleProductos d WHERE d.id = :id")
    , @NamedQuery(name = "DetalleProductos.findByNombre", query = "SELECT d FROM DetalleProductos d WHERE d.nombre = :nombre")
    , @NamedQuery(name = "DetalleProductos.findByPrecio", query = "SELECT d FROM DetalleProductos d WHERE d.precio = :precio")
    , @NamedQuery(name = "DetalleProductos.findByDescripcion", query = "SELECT d FROM DetalleProductos d WHERE d.descripcion = :descripcion")})
public class DetalleProductos implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "thumbnail_url")
    private String thumbnailUrl;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private int precio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "id_tipo_producto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TiposProducto idTipoProducto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDetalle")
    private List<Productos> productosList;

    public DetalleProductos() {
    }

    public DetalleProductos(Integer id) {
        this.id = id;
    }

    public DetalleProductos(Integer id, String nombre, int precio, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TiposProducto getIdTipoProducto() {
        return idTipoProducto;
    }

    public void setIdTipoProducto(TiposProducto idTipoProducto) {
        this.idTipoProducto = idTipoProducto;
    }

    @XmlTransient
    public List<Productos> getProductosList() {
        return productosList;
    }

    public void setProductosList(List<Productos> productosList) {
        this.productosList = productosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetalleProductos)) {
            return false;
        }
        DetalleProductos other = (DetalleProductos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "db.DetalleProductos[ id=" + id + " ]";
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
    
}
