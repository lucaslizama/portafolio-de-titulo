/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import db.DetalleProductos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author lucas
 */
@Stateless
public class DetalleProductosFacade extends AbstractFacade<DetalleProductos> {

    @PersistenceContext(unitName = "AppPortafolioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetalleProductosFacade() {
        super(DetalleProductos.class);
    }
    
}
