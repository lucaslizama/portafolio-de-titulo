/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import db.Usuarios;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

/**
 *
 * @author lucas
 */
@Stateless
public class UsuariosFacade extends AbstractFacade<Usuarios> {

    @PersistenceContext(unitName = "AppPortafolioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuariosFacade() {
        super(Usuarios.class);
    }
    
    public Usuarios findByUsername(String username) {
        TypedQuery consulta = em.createNamedQuery("Usuarios.findByUsername", Usuarios.class);
        List<Usuarios> lista = consulta.setParameter("username", username).getResultList();
        return lista.get(0);
    }
    
    public boolean ExisteUsuario(String username) {
        TypedQuery consulta = em.createNamedQuery("Usuarios.findByUsername", Usuarios.class);
        List<Usuarios> lista = consulta.setParameter("username", username).getResultList();
        return lista.size() == 1;
    }
}
