/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import db.Usuarios;
import ejb.UsuariosFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.mindrot.jbcrypt.BCrypt;

/**
 *
 * @author lucas
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    @EJB
    UsuariosFacade uf;
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        RequestDispatcher rd;
        if(session.getAttribute("LoggedUser") == null)
            rd = request.getRequestDispatcher("login.jsp");
        else
            rd = request.getRequestDispatcher("index.jsp");
        rd.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Validar campos
        for(String nombre : Collections.list(request.getParameterNames())){
            if(request.getParameter(nombre).isEmpty()) {
                forwardError("Todos los campos deben llenarse!", request, response);
                return;
            }
        }
        
        //Comprobar que exista el usuario
        if(!uf.ExisteUsuario(request.getParameter("username"))){
            forwardError("El usuario ingresado no se encuentra registrado", request, response);
            return;
        }
        
        //Buscar al usuario
        Usuarios user = uf.findByUsername(request.getParameter("username"));
        
        //Validar Password Hasheada
        if(!BCrypt.checkpw(request.getParameter("password"), user.getPass())){
            forwardError("Usuario o contraseña incorrectos", request, response);
            return;
        }
        
        //Login y redireccion a index
        request.getSession(true).setAttribute("LoggedUser", user);
        response.sendRedirect("/index");
    }

    private void forwardError(String mensaje, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("mensaje", mensaje);
        request.setAttribute("color", "red");
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
